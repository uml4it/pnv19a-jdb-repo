-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2016 at 05:26 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jdb_pactice`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkLogin`(
    IN input_username VARCHAR(255),
    IN input_password VARCHAR(255),
    OUT result VARCHAR(255)
)
BEGIN
    /* Declare flag variable default is -1 */
    DECLARE flag INT(11) DEFAULT -1;
    DECLARE user_role VARCHAR(45) DEFAULT '';
     
    /* Query isActive, role and set into flag and role */
    SELECT  isActive, u_role INTO flag, user_role FROM users
    WHERE username = input_username AND password = MD5(input_password);
 
    /* If the select statement return no result, flag = -1
       Else flag = isActive      
    */   
    
    IF (flag <= 0) THEN
            SET result = 'Login information is wrong';
    ELSEIF (flag = 0) THEN
            SET result = 'The account is disable';
    ELSEIF (flag = 1) THEN
            If ( user_role = 'Admin' ) THEN
              BEGIN
               SET result = 'This user is admin';
              END;
            ELSEIF (user_role = 'Sale' ) THEN
               SET result = 'This user is sale';
            ELSE  
               SET result = 'This user is unknown';
            END IF;
    END IF;
    
   
    
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `MYUSER` varchar(30) NOT NULL,
  `EMAIL` varchar(30) DEFAULT NULL,
  `WEBPAGE` varchar(100) NOT NULL,
  `DATUM` date NOT NULL,
  `SUMMARY` varchar(40) NOT NULL,
  `COMMENTS` varchar(400) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `MYUSER`, `EMAIL`, `WEBPAGE`, `DATUM`, `SUMMARY`, `COMMENTS`) VALUES
(1, 'lars', 'myemail@gmail.com', 'http://www.pnv.com', '2009-09-14', 'Summary', 'My first comment'),
(4, 'thientt', 'thientt@gmail.com', 'pnv.com', '2015-08-25', 'sumary', 'this is my comment');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `s_code` varchar(45) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `s_code`, `point`, `phone`) VALUES
(1, 'thien1', 'DEV1', 23, '43221'),
(2, 'Thanh', 'DEV2', 21, '3343921380'),
(3, 'sdfs', 'sdfd', 3, '3');

-- --------------------------------------------------------

--
-- Table structure for table `students_log`
--

CREATE TABLE IF NOT EXISTS `students_log` (
  `id` int(11) NOT NULL,
  `action` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isKept` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students_log`
--

INSERT INTO `students_log` (`id`, `action`, `description`, `isKept`) VALUES
(1, 'delete', 'The student id 2 was deleted.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT NULL,
  `u_role` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `isActive`, `u_role`) VALUES
(1, 'admin', 'e99a18c428cb38d5f260853678922e03', 1, 'Admin'),
(2, 'user1', 'e99a18c428cb38d5f260853678922e03', 1, 'Teacher'),
(3, 'user2', 'e99a18c428cb38d5f260853678922e03', 1, 'Student'),
(4, 'password is abc123', 'e99a18c428cb38d5f260853678922e03', 0, 'Test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_log`
--
ALTER TABLE `students_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `students_log`
--
ALTER TABLE `students_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
