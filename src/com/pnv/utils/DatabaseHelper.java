/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.utils;

import com.mysql.jdbc.CommunicationsException;
import com.pnv.login.LoginForm;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Laptop033
 */
public class DatabaseHelper {
    
    private String username;
    private static String DATABASE_NAME = "jdb_pactice";
    private static String DATABASE_USERNAME = "root";
    private static String DATABASE_PASSWORD = "";
    private static Connection cn ;

    static {

        try {
            //tạo tên truy cập cho driver:
            Class.forName("com.mysql.jdbc.Driver");
            //tạo lệnh truy cập đến cơ sở dữ liệu
            cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + DATABASE_NAME + "?user=" + DATABASE_USERNAME + "&password=" + DATABASE_PASSWORD);
            if (cn != null) {
                System.out.println("Connected to the database jdb_pactice");
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static Connection getConnection() {
        return cn;
    }
}
