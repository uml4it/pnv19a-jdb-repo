/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.mysql.first;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class MySQLAccess {

    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public MySQLAccess() throws Exception {
        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/jdb_pactice?"
                            + "user=root&password=");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void readDataBase() throws Exception {
        try {
            resultSet = statement
                    .executeQuery("select * from comments");
            writeResultSet(resultSet);

        } catch (Exception e) {
            throw e;
        }

    }

    public void insertData(String myuser, String email, String webpage, String summary, String comment) {
        try {
            // PreparedStatements can use variables and are more efficient
            preparedStatement = connect
                    .prepareStatement("insert into  comments values (default, ?, ?, ?, ? , ?, ?)");
            // "myuser, webpage, datum, summary, COMMENTS from comments");
            // Parameters start with 1
            preparedStatement.setString(1, myuser);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, webpage);
            preparedStatement.setDate(4, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            preparedStatement.setString(5, summary);
            preparedStatement.setString(6, comment);
            preparedStatement.executeUpdate();

            preparedStatement = connect
                    .prepareStatement("SELECT myuser, webpage, datum, summary, COMMENTS from comments");

            resultSet = preparedStatement.executeQuery();

            writeResultSet(resultSet);

        } catch (SQLException ex) {
            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteData(String myuser) {
        try {
            // Remove again the insert comment
            preparedStatement = connect
                    .prepareStatement("delete from comments where myuser= ? ; ");
            preparedStatement.setString(1, myuser);
            preparedStatement.executeUpdate();

            resultSet = statement
                    .executeQuery("select * from comments");
            System.out.println("The user" + myuser + "was deleted");

            // Print all users'comment
            this.readDataBase();

        } catch (Exception ex) {
            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private void stopConnectingToDatabase() {

        String s;
        System.out.println("Do you want to stop connecting to database? (y/n) ");

        Scanner in = new Scanner(System.in);
        s = in.nextLine();
        if (s.equals("y")) {
            this.close();
        }
        
    }

    private void writeResultSet(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
            String user = resultSet.getString("myuser");
            String website = resultSet.getString("webpage");
            String summary = resultSet.getString("summary");
            Date date = resultSet.getDate("datum");
            String comment = resultSet.getString("comments");
            System.out.println("==========================================================" );
            System.out.println("User: " + user);
            System.out.println("Website: " + website);
            System.out.println("Summery: " + summary);
            System.out.println("Date: " + date);
            System.out.println("Comment: " + comment);
        }
        
        //ask if user want to stop connecting to database
        this.stopConnectingToDatabase();
    }

    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }
}
