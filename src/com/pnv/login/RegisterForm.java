/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.login;

import com.pnv.utils.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Laptop033
 */
public class RegisterForm extends javax.swing.JFrame {
    private String password;

    private Statement stmt;
    private ResultSet rs;
    private Connection cn;
    private String current_username = "";

    /**
     * Creates new form RegisterForm
     */
    public RegisterForm() {

        cn = DatabaseHelper.getConnection();
        try {
            stmt = cn.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(RegisterForm.class.getName()).log(Level.SEVERE, null, ex);
        }

        initComponents();
    }

    /**
     * Nếu là edit profile Creates new form RegisterForm
     */
    public RegisterForm(String username) {

        try {
            cn = DatabaseHelper.getConnection();
            try {
                stmt = cn.createStatement();
            } catch (SQLException ex) {
                Logger.getLogger(RegisterForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            initComponents();
            this.current_username = username;
            /**
             * chuyển nút register thành nút update
             */
            btn_submit.setText("Update");
            tf_username.disable();
            tf_re_password.setVisible(false);
            /**
             * lấy thông tin user
             */
            String sqlQuery = "SELECT * FROM users WHERE username = '" + username + "' LIMIT 1 ";
            rs = stmt.executeQuery(sqlQuery);
            if (rs.next()) {
                tf_username.setText(rs.getString("username"));
                this.tf_password.setText(rs.getString("password"));
                this.tf_re_password.setText(rs.getString("password"));
                this.tf_role.setText(rs.getString("u_role"));
                boolean isActive = (rs.getInt("isActive") == 1) ? true : false;
                this.cb_isActive.setSelected(isActive);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegisterForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tf_username = new javax.swing.JTextField();
        lb_username_error = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lb_password_error = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lb_re_password_error = new javax.swing.JLabel();
        cb_isActive = new javax.swing.JCheckBox();
        lb_role_error = new javax.swing.JLabel();
        tf_role = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btn_submit = new javax.swing.JButton();
        btn_back = new javax.swing.JButton();
        lb_message = new javax.swing.JLabel();
        tf_password = new javax.swing.JPasswordField();
        tf_re_password = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(500, 500));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Register");

        jLabel2.setText("UserName");

        lb_username_error.setForeground(new java.awt.Color(255, 0, 0));
        lb_username_error.setText("jLabel3");

        jLabel3.setText("password");

        lb_password_error.setForeground(new java.awt.Color(255, 51, 0));
        lb_password_error.setText("jLabel4");

        jLabel4.setText("re-password");

        lb_re_password_error.setForeground(new java.awt.Color(255, 51, 0));
        lb_re_password_error.setText("jLabel4");

        cb_isActive.setText("is Active");
        cb_isActive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_isActiveActionPerformed(evt);
            }
        });

        lb_role_error.setForeground(new java.awt.Color(255, 51, 0));
        lb_role_error.setText("jLabel4");

        jLabel5.setText("Role");

        btn_submit.setText("register");
        btn_submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_submitActionPerformed(evt);
            }
        });

        btn_back.setText("Back");
        btn_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_backActionPerformed(evt);
            }
        });

        lb_message.setForeground(new java.awt.Color(255, 51, 0));
        lb_message.setText("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_username, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tf_password, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tf_re_password, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cb_isActive)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_submit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_back))
                    .addComponent(lb_message)
                    .addComponent(tf_role, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lb_role_error, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lb_username_error, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                        .addComponent(lb_password_error, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lb_re_password_error, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(143, 143, 143)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_username, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb_username_error))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lb_password_error)
                    .addComponent(tf_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lb_re_password_error)
                        .addComponent(tf_re_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tf_role, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lb_role_error)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cb_isActive)
                .addGap(6, 6, 6)
                .addComponent(lb_message)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_submit)
                    .addComponent(btn_back))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cb_isActiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_isActiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_isActiveActionPerformed

    private void btn_submitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_submitActionPerformed

        try {
            Boolean isError = false;
            String username = this.tf_username.getText();
            String password = this.tf_password.getText();
            String re_password = this.tf_re_password.getText();
            String role = this.tf_role.getText();
            int isActive = this.cb_isActive.isSelected() ? 1 : 0;

            /**
             * kiểm tra tính hợp lệ của dữ liệu được nhập vào
             */
            if (username.equals("")) {
                isError = true;
                lb_username_error.setText("Please input username");
            }

            if (password.equals("")) {
                isError = true;
                lb_password_error.setText("Please input password");
            } else if (re_password.equals("")) {
                isError = true;
                lb_re_password_error.setText("Please input re-password");
            } else if (!password.equals(re_password)) {
                isError = true;
                lb_re_password_error.setText("Please input re-password is same to password");
            }

            if (role.equals("")) {
                isError = true;
                lb_role_error.setText("Please input role");
            }

            /**
             * Nếu tất cả đều đúng thì tiến hành Insert vào cơ sơ dữ liệu
             */
            if (!isError) {
                String sqlQuery = "";
                /**
                 * kiểm tra nếu có current_username thì cập nhập user ngược lại
                 * thì tạo mới user
                 */

                if (current_username.equals("")) {

                    /**
                     * kiểm tra xem đã có user này trong databasae hay chưa
                     */
                    sqlQuery = "SELECT * FROM users WHERE username = '" + username + "' ";
                    rs = stmt.executeQuery(sqlQuery);

                    if (rs.first()) {
                        isError = true;
                        lb_message.setText("this user already exist. ");
                    } else {

                        sqlQuery = "INSERT INTO users (username,password,isActive,u_role) VALUES ('"
                                + username + "','" + password + "','" + isActive + "','" + role + "') ";

                        stmt.executeUpdate(sqlQuery);
                        lb_message.setText("You have registered an account successfully");
                    }

                } else {
                    sqlQuery = "UPDATE users "
                            + "SET  password = '" + password +
                            "' ,isActive = '" + isActive +
                            "' , u_role = '" + role +
                            "' WHERE username = ' "+username+"'";

                    stmt.executeUpdate(sqlQuery);
                    lb_message.setText("You have updated an account successfully");

                }

            }

        } catch (SQLException ex) {
            Logger.getLogger(RegisterForm.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_btn_submitActionPerformed

    private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_backActionPerformed
        try {

            /**
             * nếu là edit profile thì quay lai loginSuccess form
             * (current_username = "") ngược lại quay về Login form
             */
            if (this.current_username.equals("")) {
                LoginForm loginForm = new LoginForm();
                loginForm.setVisible(true);
            } else {
                LoginSuccess loginSuccess = new LoginSuccess(this.current_username);
                loginSuccess.setVisible(true);
            }

            dispose(); //Destroy the JFrame object
        } catch (SQLException ex) {
            Logger.getLogger(RegisterForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btn_backActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegisterForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back;
    private javax.swing.JButton btn_submit;
    private javax.swing.JCheckBox cb_isActive;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel lb_message;
    private javax.swing.JLabel lb_password_error;
    private javax.swing.JLabel lb_re_password_error;
    private javax.swing.JLabel lb_role_error;
    private javax.swing.JLabel lb_username_error;
    private javax.swing.JPasswordField tf_password;
    private javax.swing.JPasswordField tf_re_password;
    private javax.swing.JTextField tf_role;
    private javax.swing.JTextField tf_username;
    // End of variables declaration//GEN-END:variables
}
